package com.avenuecode.challenge.integration;

import java.math.BigDecimal;
import java.util.Random;

import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractTransformer;

public class EmployeeBonusTransformer extends AbstractTransformer {

	@Override
	protected Object doTransform(Object src, String enc)
			throws TransformerException {
		BigDecimal bonus = null;
		
		if(src instanceof BigDecimal){
			BigDecimal salary = (BigDecimal) src;
			Random random = new Random();

			int percentvl = random.nextInt(50);
			Long result = ((salary.longValue() * percentvl) / 100);
			bonus = bonus.valueOf(result);
			bonus = bonus.setScale(2, BigDecimal.ROUND_HALF_UP);

		}
		
		
		
		// TODO Insert the bonus implementation here!
		
		return bonus;
	}

}
