package com.avenuecode.challenge.soap;

import java.math.BigDecimal;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(name="EmployeesService", targetNamespace="http://www.avenuecode/challenge")
public interface EmployeeWS {
	
	@WebMethod(operationName="employeeBonus", action="EmployeeAction")
    @WebResult(name="bonus")
	public BigDecimal getEmployeeBonus(@WebParam(name="employeeId")Integer id);
	
	@WebMethod(operationName="avgEmployeesSalaries", action="EmployeesSalariesAction")
    @WebResult(name="avarage")
	public BigDecimal getEmployeesSalaries();
}
